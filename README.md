Our mission at Shelf is to help people consume mindfully. We believe we can reduce waste and delight website visitors by guiding them to the right product for their needs.

Website : https://www.shelf.guide/
